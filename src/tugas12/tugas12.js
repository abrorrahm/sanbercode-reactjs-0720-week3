import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: 0,
      date: new Date()
    }
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: this.state.time - 1,
      date: new Date() 
    });
  }


  render(){
    return(
      <>
      {this.state.time >= 1 && 
      <table style={{border: "0px solid white"}}>
          <tr>
              <th style={{background: "white", textAlign: "left", margin: "auto"}}>sekarang jam : {this.state.date.toLocaleTimeString()}</th>
              <th style={{background: "white", textAlign: "right"}}>hitung mundur : {this.state.time}</th>
          </tr>
      </table>
      }
      </>
    )
  }
}

export default Timer