import React, {Component} from "react"
import "./tugas13.css"

class Lists extends Component{

  constructor(props){
    super(props)
    this.state = {
      dataHargaBuah : [ {nama: "Semangka", harga: 10000, berat: 1000},
      {nama: "Anggur", harga: 40000, berat: 500},
      {nama: "Strawberry", harga: 30000, berat: 400},
      {nama: "Jeruk", harga: 30000, berat: 1000},
      {nama: "Mangga", harga: 30000, berat: 500} ],
      inputNama: "", 
      inputHarga: "", 
      inputBerat: "",
      indexOfForm: -1    
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
 
  handleDelete(event){
    let index = event.target.value
    let newDataBuah = this.state.dataHargaBuah
    let editedDataBuah = newDataBuah[this.state.index]
    newDataBuah.splice(index, 1)

    if (editedDataBuah !== undefined){
      // array findIndex baru ada di ES6
      var newIndex = newDataBuah.findIndex((el) => el === editedDataBuah)
      this.setState({
        dataHargaBuah: newDataBuah, 
        index: newIndex
      })
    }else{
      this.setState({dataHargaBuah: newDataBuah})
    }
  }
  
  handleEdit(event){
    let index = event.target.value
    let buah = this.state.dataHargaBuah[index]
    this.setState({
      inputNama:buah.nama, 
      inputHarga:buah.harga, 
      inputBerat:buah.berat, 
      indexOfForm:index
    })
  }

  handleChange(event){
    let input = event.target.name;
        if( input === "nama" ){
            this.setState({inputNama: event.target.value});
        }else if( input === "harga" ){
            this.setState({inputHarga: event.target.value});
        }else if( input === "berat" ){
            this.setState({inputBerat: event.target.value});
        }
  }

  handleSubmit(event){
    // menahan submit
    event.preventDefault()

    let nama = this.state.inputNama
    let harga = this.state.inputHarga
    let berat = this.state.inputBerat

    if (nama.replace(/\s/g,'') !== "" && harga.toString().replace(/\s/g,'') !== "" && berat.toString().replace(/\s/g,'') !== ""){      
      let newDataBuah = this.state.dataHargaBuah
      let index = this.state.indexOfForm
      
      if (index === -1){
        newDataBuah = [...newDataBuah, 
          {
            nama: this.state.inputNama,
            harga: this.state.inputHarga,
            berat: this.state.inputBerat
          }]
      }else{
        newDataBuah[index] = {
          nama, 
          harga, 
          berat
        }
      }
  
      this.setState({
        dataHargaBuah: newDataBuah,
        inputNama: "", 
        inputHarga: "", 
        inputBerat: "",
        indexOfForm: -1
      })
    }
  }

  render(){
    return(
      <>
      <div className='table'>
      <h1>Tabel Harga Buah</h1>
        <table>
          <thead>
            <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
              {
                this.state.dataHargaBuah.map((val, index)=>{
                  return(                    
                    <tr key={index}>
                      <td>{val.nama}</td>
                      <td>{val.harga}</td>
                      <td>{val.berat/1000} Kg</td>
                      <td>
                        <button onClick={this.handleEdit} value={index}>Edit</button>
                        &nbsp;
                        <button onClick={this.handleDelete} value={index}>Delete</button>
                      </td>
                    </tr>
                  )
                })
              }
          </tbody>
        </table>
      </div>
        {/* Form */}
        <h1>Form Tambah Data Buah</h1>
        <div className='form'>
          <form onSubmit={this.handleSubmit}>
            <label>
              Masukkan Nama Buah:
            </label>          
            <input type="text" name="nama" value={this.state.inputNama} onChange={this.handleChange}/>&nbsp;
            <label>
              Masukkan Harga Buah:
            </label>          
            <input type="text" name="harga" value={this.state.inputHarga} onChange={this.handleChange}/>&nbsp;
            <label>
              Masukkan Berat Buah:
            </label>          
            <input type="text" name="berat" value={this.state.inputBerat} onChange={this.handleChange}/><br></br><br></br>
            <button>submit</button><br></br>
          </form>
        </div>
      </>
    )
  }
}

export default Lists
