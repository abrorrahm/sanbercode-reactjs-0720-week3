import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import DataHargaBuah from '../tugas11/HargaBuah'
import Timer from '../tugas12/tugas12'
import Lists from '../tugas13/tugas13'
import DaftarBuah from '../tugas14/tugas14'
import HargaBuah from './HargaBuah'

const Routes = () => {

    return(
        <>
            <nav style = {{textAlign: "center"}}>
                <ul>
                <li style = {{display: "inline-block", marginRight: "40px"}}>
                    <Link to="/tugas11">Tugas 11</Link>
                </li>
                <li style = {{display: "inline-block", marginRight: "40px"}}>
                    <Link to="/tugas12">Tugas 12</Link>
                </li>
                <li style = {{display: "inline-block", marginRight: "40px"}}>
                    <Link to="/tugas13">Tugas 13</Link>
                </li>
                <li style = {{display: "inline-block", marginRight: "40px"}}>
                    <Link to="/tugas14">Tugas 14</Link>
                </li>
                <li style = {{display: "inline-block"}}>
                    <Link to="/">Tugas 15</Link>
                </li>
                </ul>
            </nav>
        <Switch>
            <Route path='/tugas11' component={DataHargaBuah}/>
            <Route path='/tugas12'>
                <Timer start={101}/>
            </Route>
            <Route path='/tugas13'>
                <Lists />
            </Route>
            <Route path='/tugas14'>
                <DaftarBuah />
            </Route>
            <Route path = '/'>
                <HargaBuah />
            </Route>
        </Switch>
    </>
    )
};

export default Routes;