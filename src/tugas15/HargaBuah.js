import React from "react"
import {HargaBuahProvider} from "./HargaBuahContext"
import {IdProvider} from "./IdContext"
import HargaBuahList from "./HargaBuahList"
import HargaBuahForm from "./HargaBuahForm"

const HargaBuah = () =>{
  return(
    <HargaBuahProvider>
        <IdProvider>
            <HargaBuahList />
            <HargaBuahForm />
        </IdProvider>
    </HargaBuahProvider>
  )
}

export default HargaBuah
