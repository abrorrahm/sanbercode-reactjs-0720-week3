import React, {useContext} from "react"
import {HargaBuahContext} from "./HargaBuahContext"
import {IdContext} from "./IdContext"
import axios from "axios"

const HargaBuahList = () =>{
  const [dataHargaBuah, setDataHargaBuah] = useContext(HargaBuahContext)
  const [setSelectedId] = useContext(IdContext)

  const handleDelete = (event) => {
    let idBuah = parseInt(event.target.value)

    let newDataHargaBuah = dataHargaBuah.filter(el => el.id !== idBuah)

    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
    .then(res => {
      console.log(res)
    })
          
    setDataHargaBuah([...newDataHargaBuah])
  }

  const handleEdit = (event) =>{
    let idBuah = parseInt(event.target.value)
    setSelectedId(idBuah)
  }

  return(
    <div className='table'>
        <h1>Tabel Harga Buah</h1>
          <table>
            <thead>
              <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                {
                  dataHargaBuah !== null && dataHargaBuah.map((val, index)=>{
                    return(                    
                      <tr key={index}>
                        <td>{val.nama}</td>
                        <td>{val.harga}</td>
                        <td>{val.berat/1000} Kg</td>
                        <td>
                          <button onClick={handleEdit} value={val.id}>Edit</button>
                          &nbsp;
                          <button onClick={handleDelete} value={val.id}>Delete</button>
                        </td>
                      </tr>
                    )
                  })
                }
            </tbody>
          </table>
        </div>
  )

}

export default HargaBuahList
