import React, {useState, createContext} from 'react'

export const IdContext = createContext()
export const IdProvider = props => {
    const [selectedId, setSelectedId] = useState(-1)
    return(
        <IdContext.Provider value = {[selectedId, setSelectedId]}>
            {props.children}
        </IdContext.Provider>
    )
}

export default IdContext