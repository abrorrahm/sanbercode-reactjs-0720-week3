import React from 'react';
import './App.css';
import {BrowserRouter as Router} from "react-router-dom"
import Routes from "./tugas15/Routes"

function App() {
  return (
    <div>
      <Router>
        {/* Tugas 11 */}
        {/* <HargaBuah /> */}

        {/* Tugas 13 */}
        {/* <Lists /> */}

        {/* Tugas 12 */}
        {/* <Timer start = '101'/>  */}

        {/* Tugas 14 */}
        {/* <DaftarBuah /> */}

        {/* Tugas 15 */}
        <Routes />
      </Router>
    </div>
  );
}

export default App;